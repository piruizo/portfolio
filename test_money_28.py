def test_multiplication():
    # test that you can multiply a Dollar by a number and get the right amount.
    five = Dollar(amount=5)
    assert Dollar(amount=10) == five.times(multiplier=2)
    assert Dollar(amount=15) == five.times(multiplier=3) 

def test_franc_multiplication():
    # test that you can multiply a Dollar by a number and get the right amount.
    five = Franc(amount=5)
    assert Franc(amount=10) == five.times(multiplier=2)
    assert Franc(amount=15) == five.times(multiplier=3) 

def test_equality():
    assert Dollar(5) == Dollar(5)
    assert Dollar(5) != Dollar(6)
    assert Franc(5) == Franc(5)
    assert Franc(5) != Franc(6)

# If we rewrite the 10 as 5 * 2, the duplication becomes more evident.
class Dollar:
    def __init__(self, amount):
        self._amount = amount
    
    def times(self, multiplier):
        return Dollar(self._amount * multiplier)
    
    def __eq__(self, dollar):
        return self._amount == dollar._amount

class Franc:
    def __init__(self, amount):
        self._amount = amount
    
    def times(self, multiplier):
        return Franc(self._amount * multiplier)
    
    def __eq__(self, franc):
        return self._amount == franc._amount

class Money:
    def __init__(self, amount):
        self._amount = amount
    
    def __eq__(self, other):
        return self._amount == other._amount

class Dollar(Money):
    def times(self, multiplier):
        return Dollar(self._amount * multiplier)